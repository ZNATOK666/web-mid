@extends('layout')

@section('content')

<div class="container">
	<h3>Write review</h3>
	<div class="row">
		<div class="col-md-12">
			{!! FORM::open(['route' => ['views.store']])!!}
			<div class="form-group">
				<input type="text" class="form-control">
				<br>
				<textarea name="description" id="" cols="30" rows="10" class="form-control"></textarea>
				<br>
				<button class="btn btn-success">Save</button>
			</div>
		</div>
	</div>
</div>

@endsection
